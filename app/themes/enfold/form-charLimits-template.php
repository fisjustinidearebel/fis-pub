
/**
* Gravity Wiz // Require Minimum Character Limit for Gravity Forms
* 
* Adds support for requiring a minimum number of characters for text-based Gravity Form fields.
* 
* @version   1.0
* @author    David Smith <david@gravitywiz.com>
* @license   GPL-2.0+
* @link      http://gravitywiz.com/...
* @copyright 2013 Gravity Wiz
*/
class GW_Minimum_Characters {
    
    public function __construct( $args = array() ) {
        
        // make sure we're running the required minimum version of Gravity Forms
        if( ! property_exists( 'GFCommon', 'version' ) || ! version_compare( GFCommon::$version, '1.7', '>=' ) )
            return;
        
        // set our default arguments, parse against the provided arguments, and store for use throughout the class
        $this->_args = wp_parse_args( $args, array( 
            'form_id' => false,
            'field_id' => false,
            'min_chars' => 0,
            'max_chars' => false,
            'validation_message' => false,
            'min_validation_message' => __( 'Please enter at least %s characters.' ),
            'max_validation_message' => __( 'You may only enter %s characters.' )
        ) );
        
        extract( $this->_args );
        
        if( ! $form_id || ! $field_id || ! $min_chars )
            return;
        
        // time for hooks
        add_filter( "gform_field_validation_{$form_id}_{$field_id}", array( $this, 'validate_character_count' ), 10, 4 );
        
    }
    
    public function validate_character_count( $result, $value, $form, $field ) {
        $char_count = strlen( $value );
        $is_min_reached = $this->_args['min_chars'] !== false && $char_count >= $this->_args['min_chars'];
        $is_max_exceeded = $this->_args['max_chars'] !== false && $char_count > $this->_args['max_chars'];
        if( ! $is_min_reached ) {
            $message = $this->_args['validation_message'];
            if( ! $message )
                $message = $this->_args['min_validation_message'];
            $result['is_valid'] = false;
            $result['message'] = sprintf( $message, $this->_args['min_chars'] );
        } else if( $is_max_exceeded ) {
            $message = $this->_args['max_validation_message'];
            if( ! $message )
                $message = $this->_args['validation_message'];
            $result['is_valid'] = false;
            $result['message'] = sprintf( $message, $this->_args['max_chars'] );
        }
        
        return $result;
    }
    
}
/* home field character field parameters -  */
/* PolicyNumber */
new GW_Minimum_Characters( array( 
    'form_id' => 2,
    'field_id' => 2,
    'min_chars' => 20,
    'max_chars' => 20,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* home__home_num */
new GW_Minimum_Characters( array( 
    'form_id' => 2,
    'field_id' => 5,
    'min_chars' => 11,
    'max_chars' => 11,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* home__cell_num */
new GW_Minimum_Characters( array( 
    'form_id' => 2,
    'field_id' => 47,
    'min_chars' => 11,
    'max_chars' => 11,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* home__bus_num */
new GW_Minimum_Characters( array( 
    'form_id' => 2,
    'field_id' => 46,
    'min_chars' => 11,
    'max_chars' => 11,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* home__reported_by */
new GW_Minimum_Characters( array( 
    'form_id' => 2,
    'field_id' => 53.6,
    'min_chars' => 10,
    'max_chars' => 200,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* home__desc_loss */
new GW_Minimum_Characters( array( 
    'form_id' => 2,
    'field_id' => 37,
    'min_chars' => 10,
    'max_chars' => 10000,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* home__add1 */
new GW_Minimum_Characters( array( 
    'form_id' => 2,
    'field_id' => 56.1,
    'min_chars' => 10,
    'max_chars' => 150,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* home__add2 */
new GW_Minimum_Characters( array( 
    'form_id' => 2,
    'field_id' => 56.2,
    'min_chars' => 10,
    'max_chars' => 150,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* home__city */
new GW_Minimum_Characters( array( 
    'form_id' => 2,
    'field_id' => 56.3,
    'min_chars' => 10,
    'max_chars' => 50,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* home__prov */
new GW_Minimum_Characters( array( 
    'form_id' => 2,
    'field_id' => 56.3,
    'min_chars' => 10,
    'max_chars' => 50,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* auto field character field parameters -  */

/* PolicyNumber */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 5,
    'min_chars' => 20,
    'max_chars' => 20,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* auto__HomePhoneNumber */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 56.3,
    'min_chars' => 11,
    'max_chars' => 11,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* CellPhoneNumber */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 47,
    'min_chars' => 11,
    'max_chars' => 11,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* BusPhoneNumber */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 46,
    'min_chars' => 11,
    'max_chars' => 11,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* ClaimReportedBy1 */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 4.3,
    'min_chars' => 11,
    'max_chars' => 200,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* ClaimReportedBy2 */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 4.6,
    'min_chars' => 11,
    'max_chars' => 200,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* DescriptionOfLoss */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 37,
    'min_chars' => 11,
    'max_chars' => 10000,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* DamageDescriptionToVehicle */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 22,
    'min_chars' => 11,
    'max_chars' => 2000,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* LocationOfVehicle */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 33,
    'min_chars' => 10,
    'max_chars' => 200,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* LocationOfLoss */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 50,
    'min_chars' => 10,
    'max_chars' => 150,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* ICBCClaimNumber */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 38,
    'min_chars' => 15,
    'max_chars' => 15,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* DriverContactPhoneNumber */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 60,
    'min_chars' => 11,
    'max_chars' => 11,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* DriverLicenseNumber */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 19,
    'min_chars' => 7,
    'max_chars' => 7,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* DriverStreetAddress1 */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 20.1,
    'min_chars' => 10,
    'max_chars' => 150,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* DriverStreetAddress2 */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 20.2,
    'min_chars' => 10,
    'max_chars' => 150,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* LicensePlateNumber */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 25,
    'min_chars' => 7,
    'max_chars' => 7,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* VehicleMake */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 30,
    'min_chars' => 1,
    'max_chars' => 20,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* VehicleModel */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 31,
    'min_chars' => 1,
    'max_chars' => 30,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

/* VehicleColour */
new GW_Minimum_Characters( array( 
    'form_id' => 1,
    'field_id' => 32,
    'min_chars' => 1,
    'max_chars' => 30,
    'min_validation_message' => __( 'Please enter at least %s characters.' ),
    'max_validation_message' => __( 'Please enter %s characters.' )
) );

