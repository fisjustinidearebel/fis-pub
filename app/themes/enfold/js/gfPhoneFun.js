jQuery(document).ready(function($) {

/**** Dynamic function required for Best Phone Number fields - the user must chose a best phone number and input 
the corresponding field to ensure a match is present */
	 /* Use: when user clicks a radio button  to the best number to reach them, the corresponding field must 
	 be populated with a valid number in order to proceed to the next page */

	// length error messages
	var numA = ' * Phone Number Approved';
	var numB = ' * Please enter 10 digits (example: 6045551234)';
	var numC = '';

	var polA = ' * Policy Number approved';
	var polB = ' * Please enter your Policy Number';
	// error messages live here
	var wch_home_f = $('#field_2_5 .fis-vldtn');
	var wch_cell_f = $('#field_2_47 .fis-vldtn');
	var wch_bus_f =	$('#field_2_46 .fis-vldtn');
	// but we must first give them a map home
	var wch_home_i = $('#input_2_5');
	var wch_cell_i = $('#input_2_47');
	var wch_bus_i = $('#input_2_46');
	// do not pass go without the proper phone number
	var wch_nextBtn = $('#gform_next_button_2_43');

 	var ach_home_f = $('#field_1_5 .fis-vldtn');
 	var ach_cell_f = $('#field_1_47 .fis-vldtn');
 	var ach_bus_f =	$('#field_1_46 .fis-vldtn');

 	var ach_home_i = $('#input_1_5');
 	var ach_cell_i = $('#input_1_47');
 	var ach_bus_i = $('#input_1_46');

 	var ach_nextBtn = $('#gform_next_button_1_75');
 	
	var ach_ach_driver_f = $('#field_1_60 .fis-vldtn');
	var ach_ach_driver_i = $('#input_1_60');
	var ach_nextBtn_d = $('#gform_next_button_1_55');

	/** Home Webclaim **/

	/* fill var with which value to correspond to the actual required phone number field */
		// give best-phone-number a value for the fis-api to pass through POST
 	// Home best phone localStorage
	$('#input_2_73 input').bind('change', function() {
		
		var hph = window.localStorage.setItem("hph",$('#input_2_73 input:checked').val());
		var get_hph = window.localStorage.getItem('hph');
	
		$('#input_2_73').prop('value',get_hph);
		$('#field_2_73').prop('value',get_hph);

		/* Error message display while user inputs their phone number */
		// home phone radio selection
		//home y
		if (get_hph == 1 && wch_home_i.val().length != 10 ) {
			wch_home_f.text(numB);
			wch_nextBtn.hide();
		} else if ( get_hph == 1 && wch_home_i.val().length == 10 ) {
			wch_home_f.text(numA);
			wch_nextBtn.show();
		} else if ( get_hph != 1 && wch_home_i.val().length == 10 ) {
			wch_home_f.empty();
		} else if ( get_hph != 1 && wch_home_i.val().length != 10 ) {
			wch_home_f.empty();
		}

		//cell y
		if (get_hph == 2 && wch_cell_i.val().length != 10 ) {
			wch_cell_f.text(numB);
			wch_nextBtn.hide();
		} else if ( get_hph == 2 && wch_cell_i.val().length == 10 ) {
			wch_cell_f.text(numA);
			wch_nextBtn.show();
		} else if ( get_hph != 2 && wch_cell_i.val().length == 10 ) {
			wch_cell_f.empty();
		} else if ( get_hph != 2 && wch_cell_i.val().length != 10 ) {
			wch_cell_f.empty();
		}

		//bus y
		if (get_hph == 3 && wch_bus_i.val().length != 10 ) {
			wch_bus_f.text(numB);
			wch_nextBtn.hide();
		} else if ( get_hph == 3 && wch_bus_i.val().length == 10 ) {
			wch_bus_f.text(numA);
			wch_nextBtn.show();
		} else if ( get_hph != 3 && wch_bus_i.val().length == 10 ) {
			wch_bus_f.empty();
		} else if ( get_hph != 3 && wch_bus_i.val().length != 10 ) {
			wch_bus_f.empty();
		}
		// console.log(get_hph);

	});

	var hph = window.localStorage.setItem("hph",$('#input_2_73 input:checked').val());
	var get_hph = window.localStorage.getItem('hph');
	// console.log(hph);

	// Phone number input field length and error message validation
	// on Keypresses within phone input fields
	// home

	$(wch_home_i).focusin(function(){
		var get_hph = window.localStorage.getItem('hph');
		$(this).prop('maxlength','10');
		wch_home_i.on('keydown keyup',function(event) {
			if($(wch_home_i).val().length == 10 && get_hph == 1) {
				wch_home_f.text(numA);
				wch_nextBtn.show();
			} else if ($(wch_home_i).val().length == 10 && get_hph != 1) {
				wch_home_f.text(numA);
			} else if (wch_home_i.val().length == 0 && get_hph == 1) {
				wch_home_f.text(numB);
			} else if (wch_home_i.val().length == 0 && get_hph != 1) {
				wch_home_f.text(numC);
			} else if (wch_home_i.val().length != 10 && get_hph != 1) {
				wch_home_f.text(numC);
			} else if (wch_home_i.val().length != 10 && get_hph == 1) {
				wch_home_f.text(numB);
				wch_nextBtn.hide();
			}
		});
	});
	// cell
	$(wch_cell_i).focusin(function(){
		var get_hph = window.localStorage.getItem('hph');
		$(this).prop('maxlength','10');
		wch_cell_i.on('keydown keyup',function(event) {
			$(this).prop('maxlength','10');
			if($(wch_cell_i).val().length == 10 && get_hph == 2) {
				wch_cell_f.text(numA);
				wch_nextBtn.show();
			} else if (wch_cell_i.val().length == 10 && get_hph != 2) {
				wch_cell_f.text(numA);
			} else if (wch_cell_i.val().length == 0 && get_hph == 2) {
				wch_cell_f.text(numB);
			} else if (wch_cell_i.val().length == 0 && get_hph != 2) {
				wch_cell_f.text(numC);
			} else if (wch_cell_i.val().length != 10 && get_hph != 2) {
				wch_cell_f.text(numC);
			} else if (wch_cell_i.val().length != 10 && get_hph == 2) {
				wch_cell_f.text(numB);
				wch_nextBtn.hide();
			}
		});
	});
	// bus
	$(wch_bus_i).focusin(function(){
		var get_hph = window.localStorage.getItem('hph');
		$(this).prop('maxlength','10');
		wch_bus_i.on('keydown keyup',function(event) {
			$(this).prop('maxlength','10');
			if($(wch_bus_i).val().length == 10 && get_hph == 3) {
				wch_bus_f.text(numA);
				wch_nextBtn.show();
			} else if (wch_bus_i.val().length == 10 && get_hph != 3) {
				wch_bus_f.text(numA);
			} else if (wch_bus_i.val().length == 0 && get_hph == 3) {
				wch_bus_f.text(numB);
			} else if (wch_bus_i.val().length == 0 && get_hph != 3) {
				wch_bus_f.text(numC);
			} else if (wch_bus_i.val().length != 10 && get_hph != 3) {
				wch_bus_f.text(numC);
			} else if (wch_bus_i.val().length != 10 && get_hph == 3) {
				wch_bus_f.text(numB);
				wch_nextBtn.hide();
			}
		});
	});

	// console.log(hph_ch);
	if (get_hph == 1) {
		$('#input_2_73').prop('value','1');
		$('#field_2_73').prop('value','1');
		$("#choice_2_73_0").prop( 'checked', true);
		$("#choice_2_73_1").prop( 'checked', false);
		$("#choice_2_73_2").prop( 'checked', false);
		// console.log('1 checked');
		// console.log(get_hph);

	} else if (get_hph == 2) {
		$('#input_2_73').prop('value','2');
		$('#field_2_73').prop('value','2');
		$("#choice_2_73_0").prop( 'checked', false);
		$("#choice_2_73_1").prop( 'checked', true);
		$("#choice_2_73_2").prop( 'checked', false);
		// console.log('2 checked');
		// console.log(get_hph);
	} else if (get_hph == 3) {
		$('#input_2_73').prop('value','3');
		$('#field_2_73').prop('value','3');
		$("#choice_2_73_0").prop( 'checked', false);
		$("#choice_2_73_1").prop( 'checked', false);
		$("#choice_2_73_2").prop( 'checked', true);
		// console.log('3 checked');
		// console.log(get_hph);
	} else if (get_hph == 0){
		// console.log('non hph');
	}

	//
	/** Auto Webclaim **/
	/* Make 'next' button hidden if phone number fields are present */
	//

 	var ach_home_f = $('#field_1_5 .fis-vldtn');
 	var ach_cell_f = $('#field_1_47 .fis-vldtn');
 	var ach_bus_f =	$('#field_1_46 .fis-vldtn');

 	var ach_home_i = $('#input_1_5');
 	var ach_cell_i = $('#input_1_47');
 	var ach_bus_i = $('#input_1_46');

 	var ach_nextBtn = $('#gform_next_button_1_75');

	var ach_driver_f = $('#field_1_60 .fis-vldtn');
	var ach_driver_i = $('#input_1_60');
	var ach_nextBtn_d = $('#gform_next_button_1_55');

	/* fill var with which value to correspond to the actual required phone number field */
		// give best-phone-number a value for the fis-api to pass through POST
 	// Home best phone localStorage
	$('#input_1_73 input').bind('change', function() {

		var aph = window.localStorage.setItem("aph",$('#input_1_73 input:checked').val());
		var get_aph = window.localStorage.getItem('aph');
	
		$('#input_1_73').prop('value',get_aph);
		$('#field_1_73').prop('value',get_aph);

		/* Error message display while user inputs their phone number */
		// auto phone radio selection
			//home y
		if (get_aph == 1 && ach_home_i.val().length != 10 ) {
			// console.log('home 1 txt');
			ach_home_f.text(numB);
			ach_nextBtn.hide();
		} else if ( get_aph == 1 && ach_home_i.val().length == 10 ) {
			ach_home_f.text(numA);
			ach_nextBtn.show();
		} else if ( get_aph != 1 && ach_home_i.val().length == 10 ) {
			ach_home_f.empty();
		} else if ( get_aph != 1 && ach_home_i.val().length != 10 ) {
			ach_home_f.empty();
		}

		//cell y
		if (get_aph == 2 && ach_cell_i.val().length != 10 ) {
			// console.log('home 2txt');
			ach_cell_f.text(numB);
			ach_nextBtn.hide();
		} else if ( get_aph == 2 && ach_cell_i.val().length == 10 ) {
			ach_cell_f.text(numA);
			ach_nextBtn.show();
		} else if ( get_aph != 2 && ach_cell_i.val().length == 10 ) {
			ach_cell_f.empty();
		} else if ( get_aph != 2 && ach_cell_i.val().length != 10 ) {
			ach_cell_f.empty();
		}

		//bus y
		if (get_aph == 3 && ach_bus_i.val().length != 10 ) {
			// console.log('home 3 txt');
			ach_bus_f.text(numB);
			ach_nextBtn.hide();
		} else if ( get_aph == 3 && ach_bus_i.val().length == 10 ) {
			ach_bus_f.text(numA);
			ach_nextBtn.show();
		} else if ( get_aph != 3 && ach_bus_i.val().length == 10 ) {
			ach_bus_f.empty();
		} else if ( get_aph != 3 && ach_bus_i.val().length != 10 ) {
			ach_bus_f.empty();
		} 

	});

	var aph = window.localStorage.setItem("aph",$('#input_1_73 input:checked').val());
	var get_aph = window.localStorage.getItem('aph');

	// Phone number input field length and error message validation
	// home

	$(ach_home_i).focusin(function(){
		var get_aph = window.localStorage.getItem('aph');
		$(this).prop('maxlength','10');
		ach_home_i.on('keydown keyup',function(event) {
			if($(ach_home_i).val().length == 10 && get_aph == 1) {
				ach_home_f.text(numA);
				ach_nextBtn.show();
			} else if (ach_home_i.val().length == 10 && get_aph != 1) {
				ach_home_f.text(numA);
			} else if (ach_home_i.val().length == 0 && get_aph == 1) {
				ach_home_f.text(numB);
			} else if (ach_home_i.val().length == 0 && get_aph != 1) {
				ach_home_f.text(numC);
			} else if (ach_home_i.val().length != 10 && get_aph != 1) {
				ach_home_f.text(numC);
			} else if (ach_home_i.val().length != 10 && get_aph == 1) {
				ach_home_f.text(numB);
				ach_nextBtn.hide();
			}
		});
	});

	// cell
	$(ach_cell_i).focusin(function(){
		var get_aph = window.localStorage.getItem('aph');
		$(this).prop('maxlength','10');
		ach_cell_i.on('keydown keyup',function(event) {
			if($(ach_cell_i).val().length == 10 && get_aph == 2) {
				ach_cell_f.text(numA);
				ach_nextBtn.show();
			} else if (ach_cell_i.val().length == 10 && get_aph != 2) {
				ach_cell_f.text(numA);
			} else if (ach_cell_i.val().length == 0 && get_aph == 2) {
				ach_cell_f.text(numB);
			} else if (ach_cell_i.val().length == 0 && get_aph != 2) {
				ach_cell_f.text(numC);
			} else if (ach_cell_i.val().length != 10 && get_aph != 2) {
				ach_cell_f.text(numC);
			} else if (ach_cell_i.val().length != 10 && get_aph == 2) {
				ach_cell_f.text(numB);
				ach_nextBtn.hide();
			}
		});
	});

	// bus
	$(ach_bus_i).focusin(function(){
		var get_aph = window.localStorage.getItem('aph');
		$(this).prop('maxlength','10');
		ach_bus_i.on('keydown keyup',function(event) {
			if($(ach_bus_i).val().length == 10 && get_aph == 3) {
				ach_bus_f.text(numA);
				ach_nextBtn.show();
			} else if (ach_bus_i.val().length == 10 && get_aph != 3) {
				ach_bus_f.text(numA);
			} else if (ach_bus_i.val().length == 0 && get_aph == 3) {
				ach_bus_f.text(numB);
			} else if (ach_bus_i.val().length == 0 && get_aph != 3) {
				ach_bus_f.text(numC);
			} else if (ach_bus_i.val().length != 10 && get_aph != 3) {
				ach_bus_f.text(numC);
			} else if (ach_bus_i.val().length != 10 && get_aph == 3) {
				ach_bus_f.text(numB);
				ach_nextBtn.hide();
			}
		});
	});

	// Driver Contact phone number

	var numAD = ' Phone Number Approved';
	var numBD = ' Please enter 10 digits (example: 6045551234)';
	$(ach_ach_driver_i).prop('type','number');
	ach_ach_driver_i.on('keydown keyup',function(event) {
		if($(this).val().length == 10) {
			ach_driver_f.text(numAD);
			$(this).prop('maxlength',10);
			nextBtn_d.show();
		} else if (ach_driver_i.val().length <= 10 ) {
			ach_driver_f.text(numBD);
			nextBtn_d.hide();
		} else if (ach_driver_i.val().length >= 10 ) {
			nextBtn_d.hide();
			ach_driver_f.text(numBD);
		}
	});

	

	// First/Lase Name
	//home > loss information
	var hname1_f1 = $('#input_2_53_3_container label');
	var hname1_f2 = $('#input_2_53_6_container label');
	var hname1_1 = $('#input_2_53_3');
	var hname1_2 = $('#input_2_53_6');
	var hname1_B = $('#gform_next_button_2_57');

	var hname1_Y = 'First Name * Max of 100 letters';
	var hname2_Y = 'Last Name * Max of 100 letters';
	var hname1_N = 'First Name';
	var hname2_N = 'Last Name';
	
	hname1_1.prop('maxlength',100);
	hname1_2.prop('maxlength',100);

	hname1_1.on('keydown',function(event) {
		if($(this).val().length == 100) {
			hname1_f1.text(hname1_Y);
		} else if ($(this).val().length != 100) {
			hname1_f1.text(hname1_N);
		}
	});

	hname1_2.on('keydown',function(event) {
		if($(this).val().length == 100) {
			hname1_f2.text(hname2_Y);
		} else if ($(this).val().length != 100) {
			hname1_f2.text(hname2_N);
		}
	});

	// Model Year
	var mod_f = $('#field_1_29 .fis-vldtn');
	var mod_i = $('#input_1_29');
	var mod_d = $('#gform_next_button_1_43');

	var numMY = ' Model Year Approved';
	var numMD = ' Please enter 4 digits (example: 1979)';

	$(mod_i).prop('type','number');
	mod_i.on('keydown',function(event) {
		if($(this).val().length == 4) {
			mod_f.text(numMY);
			mod_i.prop('maxlength',4);
			mod_d.show();
		} else if (mod_i.val().length <= 4 ) {
			mod_f.text(numMD);
			mod_d.hide();
		} else if (mod_i.val().length >= 4 ) {
			mod_d.hide();
			mod_f.text(numMD);
		}
	});

	//autoclaim > loss information
	var aname1_f1 = $('#input_1_4_3_container label');
	var aname1_f2 = $('#input_1_4_6_container label');
	var aname1_1 = $('#input_1_4_3');
	var aname1_2 = $('#input_1_4_6');
	var name1_B = $('#gform_next_button_1_75');

	var aname1_Y = 'First Name * Max of 100 letters';
	var aname2_Y = 'Last Name * Max of 100 letters';
	var aname1_N = 'First Name';
	var aname2_N = 'Last Name';
	
	aname1_1.prop('maxlength',100);
	aname1_2.prop('maxlength',100);

	aname1_1.on('keydown',function(event) {
		if($(this).val().length == 100) {
			aname1_f1.text(aname1_Y);
		} else if ($(this).val().length != 100) {
			aname1_f1.text(aname1_N);
		}
	});

	aname1_2.on('keydown',function(event) {
		if($(this).val().length == 100) {
			aname1_f2.text(aname2_Y);
		} else if ($(this).val().length != 100) {
			aname1_f2.text(aname2_N);
		}
	});

	//autoclaim > policy holder information
	var a2name1_f1 = $('#input_1_79_3_container label');
	var a2name1_f2 = $('#input_1_79_6_container label');
	var a2name1_1 = $('#input_1_79_3');
	var a2name1_2 = $('#input_1_79_6');
	var name1_B = $('#gform_next_button_1_54');

	var a2name1_Y = 'First Name * Max of 100 letters';
	var a2name2_Y = 'Last Name * Max of 100 letters';
	var a2name1_N = 'First Name';
	var a2name2_N = 'Last Name';
	
	a2name1_1.prop('maxlength',100);
	a2name1_2.prop('maxlength',100);

	a2name1_1.on('keydown',function(event) {
		if($(this).val().length == 100) {
			a2name1_f1.text(a2name1_Y);
		} else if ($(this).val().length != 100) {
			a2name1_f1.text(a2name1_N);
		}
	});

	a2name1_2.on('keydown',function(event) {
		if($(this).val().length == 100) {
			a2name1_f2.text(a2name2_Y);
		} else if ($(this).val().length != 100) {
			a2name1_f2.text(a2name2_N);
		}
	});

	//autoclaim > driver information
	var a3name1_f1 = $('#input_1_17_3_container label');
	var a3name1_f2 = $('#input_1_17_6_container label');
	var a3name1_1 = $('#input_1_17_3');
	var a3name1_2 = $('#input_1_17_6');
	var name1_B = $('#gform_next_button_1_55');

	var a3name1_Y = 'First Name * Max of 100 letters';
	var a3name2_Y = 'Last Name * Max of 100 letters';
	var a3name1_N = 'First Name';
	var a3name2_N = 'Last Name';
	
	a3name1_1.prop('maxlength',100);
	a3name1_2.prop('maxlength',100);

	a3name1_1.on('keydown',function(event) {
		if($(this).val().length == 100) {
			a3name1_f1.text(a3name1_Y);
		} else if ($(this).val().length != 100) {
			a3name1_f1.text(a3name1_N);
		}
	});

	a3name1_2.on('keydown',function(event) {
		if($(this).val().length == 100) {
			a3name1_f2.text(a3name2_Y);
		} else if ($(this).val().length != 100) {
			a3name1_f2.text(a3name2_N);
		}
	});
});