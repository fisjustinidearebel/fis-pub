// Gravity Forms Plugin
// Written by: Justin Gaskin
/* This nifty plugin performs custom functions as per FIS requirements */



// IE8 warning modal

jQuery(document).ready(function($) {

	var isIE = /*@cc_on!@*/false;
	if(isIE){
		  $('#vendor-ie-8').css('display','block');
		  $('#body').css('overflow','hidden');
			// alert('LE working v1');
			// console.log('totally ie 1');
	}else{
		  $('#vendor-ie-8').css('display','none');
			// console.log('LE not working v1');
			// alert('LE not working v1');
	}

	if (/MSIE (\d+\.\d+);/.test(navigator.userAgent) || navigator.userAgent.indexOf("Trident/")){
		$('#vendor-ie-8').css('display','block');
			// alert('LE working v2');
			// console.log('totally ie 2');
	} else {
		$('#vendor-ie-8').css('display','none');
			// alert('LE not working v2');
	}

	if (layoutEngine.vendor === 'ie' && layoutEngine.version === 8) {
		  $('#vendor-ie-8').css('display','block');
		  $('#body').css('overflow','hidden');
			// alert('LE working v3');
			// console.log('totally ie 3');
		} else {
			// console.log('LE not working');
		  $('#vendor-ie-8').css('display','none');
			// alert('LE not working v3');
	}

	var canvas = document.createElement('canvas'), context;
	if (!canvas.getContext) {
		$('#vendor-ie-8').css('display','block');
			// console.log('LE not working v4');
		// alert('LE not working v4');	
	} else {
		// alert('nope 4');
	}

	$('.ie8-closeA, .ie8-closeBTN').click(function(e) {
		e.preventDefault();
		$(this).closest('#vendor-ie-8').hide();
	});
});

jQuery(document).ready(function($) {

	/* custom topHero banner functionality */
	var topHeroHeight = $('.home_TOPhero-container').height();

	/* TOPhero manipulation to function per FIS spec - custom banner behavior only resides here */
	$('.home__TOPhero-inner').css('height',topHeroHeight);
	$('.template-page .content').css('padding','0');
	$('.a .avia-menu-fx').css('background-color','white !important');
	$('.home__TOPhero-container').closest('.fullsize').css({
		position: 'relative',
		display: 'inline-block',
		width: '100%',
		height: '50vh'
	});
	$('.home__TOPhero-container').closest('.flex_column').css({
		position: 'relative',
		display: 'inline-block',
	    'background-repeat': 'no-repeat',
	    'background-position': '50% 50%',
	    'background-size': 'cover',
	    overflow: 'visible',
		width: '120%',
		left: '-10%',
		height: '50vh'
	});
	/* default loading behavior */
	if ($(window).width() < 768 ) {
		$('.home__TOPhero-inner').closest('.avia_code_block_0').css({
			position: 'absolute',
			display: 'inline-block',
			top: '0',
			width: '100%',
			height: '100%',
			padding: '0 3em'
		});
		$('.home__TOPhero-container').closest('.fullsize').css({
			height: '45vh'
		});
		$('.home__TOPhero-inner').closest('.av-content-full').css({
			padding: '0'
		});
	} else {
		$('.home__TOPhero-inner').closest('.avia_code_block_0').css({
			position: 'absolute',
			display: 'inline-block',
			top: '0',
			width: '100%',
			height: '100%',
			padding: '0 3em'
		});
		$('#main').has('.home__TOPhero-inner').closest('.av-content-full').css({
				'padding-top': '0px'

		});
		$('#main').has('.home__TOPhero-inner').closest('.content').css({
				'padding-top': '0px'

		});
	}
	/* resizing page behavior */
	$(window).resize(function(event) {
		/* Act on the event */
		if ($(window).width() < 768 ) {
			$('.home__TOPhero-inner').closest('.avia_code_block_0').css({
				position: 'absolute',
				display: 'inline-block',
				top: '0',
				width: '100%',
				height: '100%',
				padding: '0 3em'
			});
			$('.home__TOPhero-container').closest('.fullsize').css({
				height: '45vh'
			});
			$('.home__TOPhero-inner').closest('.av-content-full').css({
				padding: '0'
			});
		} else {
			$('.home__TOPhero-inner').closest('.avia_code_block_0').css({
				position: 'absolute',
				display: 'inline-block',
				top: '0',
				width: '100%',
				height: '100%',
				padding: '0 3em'
			});
			$('.home__TOPhero-inner').closest('.av-content-full').css({
			});
		}
	});

	// clean search results page
	if ($('[text*="[avia_"')) {
		// console.log('cleaning search');
		$('article .entry-content p').text(function (index, text) {
			return text.replace('[avia_codeblock_placeholder uid=”0″] ','');
		});
		$('article .entry-content p').text(function (index, text) {
			return text.replace('[avia_codeblock_placeholder uid=”1″] ','');
		});
		$('article .entry-content p').text(function (index, text) {
			return text.replace('[avia_codeblock_placeholder uid=”2″] ','');
		});
		$('article .entry-content p').text(function (index, text) {
			return text.replace('[avia_codeblock_placeholder uid=”3″] ','');
		});
		$('article .entry-content p').text(function (index, text) {
			return text.replace('[avia_codeblock_placeholder uid=”4″] ','');
		});
		$('article .entry-content p').text(function (index, text) {
			return text.replace('[avia_codeblock_placeholder uid=”5″] ','');
		});
		$('article .entry-content p').text(function (index, text) {
			return text.replace('[avia_codeblock_placeholder uid=”6″] ','');
		});
		$('article .entry-content p').text(function (index, text) {
			return text.replace('[avia_codeblock_placeholder uid=”7″] ','');
		});
		$('article .entry-content p').text(function (index, text) {
			return text.replace('[avia_codeblock_placeholder uid=”8″] ','');
		});
		$('article .entry-content p').text(function (index, text) {
			return text.replace('[avia_codeblock_placeholder uid=”9″] ','');
		});
	}
	/* gravity forms custom functions for FIS */
	/**** revamped pageing behavior ****/

    /* Edit Button placement within the pre-submission section title row */
    /* HomeClaims */
    $('.hform-pageEdit1').appendTo(".gform_titleRow:contains('Policy Holder Information')");
    $('.hform-pageEdit2').appendTo(".gform_titleRow:contains('Loss Information')");

    /* AutoClaims */
    $('.aform-pageEdit1').appendTo(".gform_titleRow:contains('Start Your Web Claim')");
    $('.aform-pageEdit2').appendTo(".gform_titleRow:contains('Policy Holder Information')");
    $('.aform-pageEdit3').appendTo(".gform_titleRow:contains('Driver Information')");
    $('.aform-pageEdit4').appendTo(".gform_titleRow:contains('Vehicle Information')");
    $('.aform-pageEdit5').appendTo(".gform_titleRow:contains('Loss Information & Accident Details')");

	$('.gwmpn-page-link').click(function(e){
	    e.preventDefault();
    	/* hide breadcrumbs on edit for now - todo: fix breadcrumb step to accurate visible stage */
    	$('.gf_page_steps').css('display','none');
		/* register which form and/or which page is being referenced */
		var form_page_id = $('.gform_page').prop('id').charAt(11);
		/* register clicked link as subsection to move back to in the form */
		var form_page_num = $(this).prop('href').substr(-1);
		/* count how many pages there are to the form and var  and last page */
		var form_page_lastPage = $('[id*="gform_page_'+form_page_id+'_'+form_page_num+'"]');
		var form_page_firstPage = $('[id*="gform_page_'+form_page_id+'_'+'1'+'"]');

		/* switch the end-page-data-preview of the form, while updating the end preview dynamically when user clicks to next page */
		$(this).prop('href',form_page_lastPage);
		$('[id*="gform_page_'+form_page_id+'_'+form_page_num+'"]').css('display','block');
		$('[id^="gform_page_"]:last').css('display','none');

	});	

	/* if user erases mandatory text field, hide the next button and warn the user */
	if ($('.gfield_contains_required input:text').length) {
		if ($(this).on('keyup').length == 0) {
			nextBtn.hide();
			alert('Please ensure all Mandatory fields marked with an "*" are filled appropriately');
			// console.log('delete pressed');
		}
		// console.log('required field exists');
	}

	//* gf pageing fix for radio checkbox throughput to fis-api *//
	// homeclaim
	if ($('#gform_page_2_2').is('visible')) {
		var page2 = $('#gform_page_2_2');
		var page3 = $('#gform_page_2_3');
		var page4 = $('#gform_page_2_4');


		var nextButts = $(this).child(".gform_next_button_2_43");
		$(nextButts).click(function(e) {
			e.preventDefault();
			$(page2).hide();
			$(page3).show();
			$(page4).hide();
		});
	}

	/**** privacy notice **/
	/* display privacy modal */
    $(".gf-priv__button").click(function(e){
    	e.preventDefault();
        $(".gf-priv__overlay").css('display','block');
    });
	/* close button for privacy modal */
    $(".gf-priv__close-btn").click(function(e){
    	e.preventDefault();
        $(".gf-priv__overlay").css('display','none');
    });
	    
	// max-length for fields as per FIS
	$('#input_2_4_3').prop('maxlength','100');
	$('#input_2_4_6').prop('maxlength','100');
	$('#input_1_4_3').prop('maxlength','100');
	$('#input_1_4_6').prop('maxlength','100');
	$('#input_1_79_3').prop('maxlength','100');
	$('#input_1_79_6').prop('maxlength','100');
	$('#input_1_17_3').prop('maxlength','100');
	$('#input_1_17_6').prop('maxlength','100');

	// date/time quirks for browser compatibility/functionality
	if ($('.datepicker').length) {
		/* prevent future dates selection in datepicker */
		$(".datepicker").datepicker({maxDate: '0'});
	}
	if ($('.auto_timeloss').css('display','block')) {
		/* prevent future dates selection in datepicker */
		$(".auto_timeloss").css('width','85% !important');
	}
});