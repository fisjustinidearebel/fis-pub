<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'database_name_here');

/** MySQL database username */
define('DB_USER', 'username_here');

/** MySQL database password */
define('DB_PASSWORD', 'password_here');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'FougS[Q`I98*gua)TJldnM]|&X8u /TH|(*;P<Tj#|POt]oAHLP~O{VB<<Xz4N0}');
define('SECURE_AUTH_KEY',  'W|]pBy)BO?^g0rQVNjH0R?iJQ?yjuYe?R_/JTL.yclNU{$_qQ<bV&,|o)^iC[j&1');
define('LOGGED_IN_KEY',    'j-YpN>Ft*U;pz+:]$M:OmZ0|Z.j|RCmTCt q!J_d:cT@|-N &=A|46^>GM8SNkd^');
define('NONCE_KEY',        'Tx9c;xCa||)5i6#/LeR_~^srz%4{iBw*Cb~yH6AiBsFG,5P>50rKeerO637}Kl>q');
define('AUTH_SALT',        'Hw GDoX|XimFs<|-hSk4H&%qR9FlvckDNEzg%QlnMFhN4[RY ^jSe%Aj`HHl#fBA');
define('SECURE_AUTH_SALT', 'FaU7v]v-1ISTwx8,9Zk-wR+}C%Lt=op(KFD-NO&Q KN Gd94mUNT5T|[Yewh#s]s');
define('LOGGED_IN_SALT',   'TR>!7;@y5^L{Lb #FaJi Lq$B]+6}WExZDp,L{m};S=6>+&og{H{mgC2}K|TB&+s');
define('NONCE_SALT',       '?}kl}1QaHUrxA=$6EGtYq#cmR|WZN)5/I5`-Yss2,+R1-Nb`?-{IG78|Pj!g4D~6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
